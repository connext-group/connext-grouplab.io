# Welcome to Connext Randomizer

### Visit Randomizer Page: https://connext-group.gitlab.io/

This is a simple randomizer made in HTML, JavaScript and CSS.

It is specifically created to aid in shuffling and drawing names when having a Minute to Win it! Challenges.